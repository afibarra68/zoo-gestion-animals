package com.Animales.Request.service;


import com.Animales.Request.Repository.dto.AnimalName;
import com.Animales.Request.Repository.dto.AnimalNameGender;
import com.Animales.Request.domain.Animal;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface IAnimalService {

    public ResponseEntity create (Animal animal);

    public Iterable<Animal> read();

    public Animal update(Animal animal);

    public Optional<Animal> getById(String code);

    public Integer quantityAnimals();

    public Iterable<AnimalNameGender> getAnimalNameGenre();

    public Integer quantityAnimalByGenderMale();

    public Integer quantityAnimalByGenderFemale();

    public Iterable<AnimalName> getAnimalByMale();

    public Iterable<AnimalName> getAnimalByFemale();



}

package com.Animales.Request.service;


import com.Animales.Request.Repository.AnimalRepository;
import com.Animales.Request.Repository.dto.AnimalName;
import com.Animales.Request.Repository.dto.AnimalNameGender;
import com.Animales.Request.domain.Animal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IAnimalServiceImp implements IAnimalService{


    @Autowired
    AnimalRepository animalRepository;

    @Override
    public ResponseEntity create(Animal animal) {
        if (animalRepository.findById(animal.getCode()).isPresent()){
            return new ResponseEntity("THE CODE IS IN USE", HttpStatus.BAD_REQUEST);
        }else {
            return new ResponseEntity(animalRepository.save(animal), HttpStatus.OK);
        }
    }
    @Override
    public Iterable<Animal> read() {
        return animalRepository.findAll();
    }
    @Override
    public Animal update(Animal animal) {
        return animalRepository.save(animal);
    }

    @Override
    public Optional<Animal> getById(String code) {
        return animalRepository.findById(code);
    }

    @Override
    public Integer quantityAnimals() {
        return animalRepository.quantityAnimal();
    }

    @Override
    public Iterable<AnimalNameGender> getAnimalNameGenre() {
        return animalRepository.findAnimalsNameGenre();
    }

    @Override
    public Integer quantityAnimalByGenderMale() {
        return animalRepository.quantityAnimalByGenreMale();
    }

    @Override
    public Integer quantityAnimalByGenderFemale() {
        return animalRepository.quantityAnimalGenreFemale();
    }

    @Override
    public Iterable<AnimalName> getAnimalByMale() {
        return animalRepository.listAnimalMale();
    }

    @Override
    public Iterable<AnimalName> getAnimalByFemale() {
        return animalRepository.listAnimalFemale();
    }
}

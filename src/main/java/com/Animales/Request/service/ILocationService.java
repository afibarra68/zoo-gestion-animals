package com.Animales.Request.service;

import com.Animales.Request.domain.Location;

public interface ILocationService {

    public Object create (Location location);

    public Iterable<Location> read();

}

package com.Animales.Request.service;

import com.Animales.Request.Repository.LocationRepository;
import com.Animales.Request.domain.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ILocationServiceImp implements ILocationService{

    @Autowired
    LocationRepository locationRepository;

    public ResponseEntity create(Location location) {
        if (locationRepository.findById(location.getId()).isPresent()){
            return new ResponseEntity("THE CODE IS IN USE", HttpStatus.BAD_REQUEST);
        }else {
            return new ResponseEntity(locationRepository.save(location), HttpStatus.OK);
        }
    }

    public Iterable<Location> read() {
        return locationRepository.findAll();
    }
}

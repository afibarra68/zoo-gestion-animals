package com.Animales.Request.service;

import com.Animales.Request.domain.LocationType;
import org.springframework.http.ResponseEntity;

public interface ILocationTypeService {
    public ResponseEntity create (LocationType locationType);

    public Iterable<LocationType> read();
}

package com.Animales.Request.service;

import com.Animales.Request.Repository.LocationTypeRepository;
import com.Animales.Request.domain.LocationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ILocationTypeServiceImp  implements ILocationTypeService{

    @Autowired
    LocationTypeRepository locationTypeRepository;

    @Override
    public ResponseEntity create(LocationType locationType) {
        if (locationTypeRepository.findById(locationType.getId()).isPresent()){
            return new ResponseEntity("El Response ", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(locationTypeRepository.save(locationType), HttpStatus.OK);
    }

    @Override
    public Iterable<LocationType> read() {
        return locationTypeRepository.findAll();
    }
}

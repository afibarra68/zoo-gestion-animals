package com.Animales.Request.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;
    private String description;

    @ManyToOne
    private LocationType locationType;
    public Location(String description, LocationType locationType) {
        this.description = description;
        this.locationType = locationType;
    }

}

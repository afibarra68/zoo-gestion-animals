package com.Animales.Request.domain;

import com.Animales.Request.domain.Enumeration.Genre;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class Animal {
    @Id
    private String code;
    private String name;
    private String race;
    @Enumerated(EnumType.STRING)
    private Genre genre;
    @ManyToOne
    private Location location;

}

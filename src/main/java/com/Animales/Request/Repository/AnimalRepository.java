package com.Animales.Request.Repository;

import com.Animales.Request.Repository.dto.AnimalName;
import com.Animales.Request.Repository.dto.AnimalNameGender;
import com.Animales.Request.domain.Animal;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface AnimalRepository extends CrudRepository<Animal, String> {

    @Query("select count(animal) from Animal animal" )
    Integer quantityAnimal();

    @Query(value = "select animal.name as name, animal.genre as genre from Animal animal")
    Iterable<AnimalNameGender> findAnimalsNameGenre();

    @Query(value = "select count(genre) from Animal animal where animal.genre = 'MACHO'")
    Integer quantityAnimalByGenreMale();

    @Query(value = "select count(genre) from Animal animal where animal.genre = 'MACHO'")
    Integer quantityAnimalGenreFemale();

    @Query(value = "select animal.name as name, animal.genre as genre from Animal animal where genre= 'MACHO'")
    Iterable<AnimalName> listAnimalMale();

    @Query(value = "select animal.name as name, animal.genre as genre from Animal animal where genre= 'HEMBRA'")
    Iterable<AnimalName> listAnimalFemale();



}

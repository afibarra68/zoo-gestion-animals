package com.Animales.Request.Repository.dto;

public interface AnimalNameGender {

    public String getName();
    public String getGenre();
}

package com.Animales.Request.Repository;

import com.Animales.Request.domain.LocationType;
import org.springframework.data.repository.CrudRepository;

public interface LocationTypeRepository extends CrudRepository<LocationType, Integer> {
}

package com.Animales.Request.Repository;

import com.Animales.Request.domain.Location;
import org.springframework.data.repository.CrudRepository;

public interface LocationRepository extends CrudRepository<Location, Integer> {
}

package com.Animales.Request.web.rest;

import com.Animales.Request.domain.Location;
import com.Animales.Request.service.ILocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class LocationResource {

    @Autowired
    ILocationService iUbicationService;

    @PostMapping("/l-r")
    public ResponseEntity create(@RequestBody Location location) {
        return (ResponseEntity) iUbicationService.create(location);
    }
    //Get @ManyToOne

    @GetMapping("/l-r")
    public Iterable<Location> read(){
        return iUbicationService.read();
    }
}

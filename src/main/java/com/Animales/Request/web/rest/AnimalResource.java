package com.Animales.Request.web.rest;


import com.Animales.Request.Repository.dto.AnimalName;
import com.Animales.Request.Repository.dto.AnimalNameGender;
import com.Animales.Request.domain.Animal;

import com.Animales.Request.service.IAnimalServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class AnimalResource {

    @Autowired
    IAnimalServiceImp iAnimalService;

    //GetMapping/CreateAnimal
    @GetMapping("/create-a")
    public Iterable<Animal> read() {
        return iAnimalService.read();
    }
    @PostMapping("/create-a")
     public ResponseEntity create(@RequestBody Animal animal) {
        return (ResponseEntity) iAnimalService.create(animal);
    }

    //GetMapping/CountAnimal
    @GetMapping("/count-a")
    public Integer countAnimals(){
        return iAnimalService.quantityAnimals();
    }

    //GetMapping/CountByGenderMale
    @GetMapping("/count/genderMale")
    public Integer countAnimalsByGenderMale(){
        return  iAnimalService.quantityAnimalByGenderMale();
    }

    //GetMapping/CountByGenderFemale
    @GetMapping("/count/genderFemale")
    public Integer countAnimalsByGenderFemale(){
        return iAnimalService.quantityAnimalByGenderFemale();
    }

    //List/NameGender
    @GetMapping("/name-genre")
    public Iterable<AnimalNameGender> getAnimalNameGenre() {
        return iAnimalService.getAnimalNameGenre();
    }

    //Put Method
    @PutMapping("/create-a")
    public Animal update(@RequestBody Animal animal) {
        return iAnimalService.update(animal);
    }

    @GetMapping("/list/genderMale")
    public Iterable<AnimalName> listAnimalMale(){
        return iAnimalService.getAnimalByMale();
    }

    @GetMapping("/list/genderFemale")
    public Iterable<AnimalName> listAnimalByFemale(){
        return iAnimalService.getAnimalByFemale();
    }

}

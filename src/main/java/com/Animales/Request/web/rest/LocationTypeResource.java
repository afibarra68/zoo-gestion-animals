package com.Animales.Request.web.rest;

import com.Animales.Request.domain.Location;
import com.Animales.Request.domain.LocationType;
import com.Animales.Request.service.ILocationService;
import com.Animales.Request.service.ILocationTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class LocationTypeResource {

    @Autowired
    ILocationTypeService iLocationTypeService;
    //
    @PostMapping("/l-t")
    public ResponseEntity create(@RequestBody LocationType locationType) {
        return (ResponseEntity) iLocationTypeService.create(locationType);
    }
    //Get @ManyToOne
    @GetMapping("/l-t")
    public Iterable<LocationType> read(){
        return iLocationTypeService.read();
    }
}
